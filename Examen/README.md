# Examen 1SYSD 

Livrable : lien vers le répertoire `Examen` d'un dépôt GIT public. 
(n'oubliez pas d'exécuter `git add` sur vos fichiers (pas les binaires !),
`git commit -a -m "..."` et `git push` avant de quitter la salle !

## Fahrenheit et Celsius

_Fichier à fournir :_ `tconvert.c`

Il existe deux unités de mesure de température communément utilisées :
le degré Celsius °C (d'usage dans une majorité de pays) et le degré
Fahrenheit °C d'usage en particulier aux E.-U. d'Amérique.

Si _C_ est une température mesurée en degrés Celius, la température
correspondante en degrés Fahrenheit est donnée par la formule : 

F = (9/5) * C + 32

La formule de conversion inverse est donc :

C = (F - 32)*5/9

Écrire en C les deux fonctions de conversion : `celsius2fahrenheit` et
`fahrenheit2celsius` qui acceptent un argument de type `float`et renvoient
une valeur du même type.

Dans le programme principal demander à l'utilisateur s'il souhaite
convertir de °C à °F (saisie du caractère `1`) ou l'inverse 
(saisie du caractère `2`), puis demande la saisie d'un nombre et
affiche le résultat de la conversion.

## Compter des caractères

_Fichiers à fournir :_ `countchar1.c` et `countchar2.c`

1. Écrire une fonction `count_char` qui accepte deux arguments : une
chaîne de caractère et un caractère et renvoie combien de fois
le caractère est présent dans la chaîne.

Dans le programme principal traiter le second argument reçu
comme le caractère à dénombrer et le premier comme la chaîne
à examiner. Exemple d'usage :

~~~~Bash
$ ./countchar1 "to be or not to be" "o"
4
$ ./countchar1 "to be or not to be" "O"
0
$ ./countchar1 "to be or not to be" "z"
0
~~~~

2. Ajouter un argument optionnel `-i` en dernière position qui rend le
comptage insensible à la casse (i.e. compter 'a' ou 'A' indifféremment
si le caractère à dénomber est 'a' ou 'A'). Exemple d'usage :

~~~~Bash
$ ./countchar2 "to be or not to be" "o"
4
$ ./countchar2 "to be or not tO be" "O"
1
$ ./countchar2 "to be or not tO be" "o" -i
4
$ ./countchar2 "to be or not tO be" "O" -i
4
$ ./countchar2 "to be or not to be" "z" -i
0
~~~~

## Inversions

1. Écrire un programme (source `revert.c`) qui affiche les arguments qu'il a reçus en ordre inverse :

~~~~Bash
$ ./revert hello goodbye foo
foo
goodbye
hello
~~~~

2. Modifiez le programme (source `revert2.c`) pour que, de plus, il inverse
l'ordre des caractères des arguments reçus :

~~~~Bash
$ ./revert hello goodbye foo
oof
eybdoog
olleh
~~~~

## Compter des nombres

_Fichier à fournir :_ `countint.c`

Écrire une fonction `compte_int`qui accepte trois arguments : un
tableau de valeurs de type `int`, la taille du tableau et une valeur.
La fonction renvoie le nombre de fois où l'on trouve la valeur
dans le tableau.

Dans le programme principal tester le bon fonctionnement de
la fonction ainsi :

~~~~C

int main() {
    int t1[5] = { 2, 42, 1, 42, 9 };
    int t2[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };


    printf("%d dans t1 : %d fois.\n", 42, compte_int(t1, 5, 42));
    printf("%d dans t1 : %d fois.\n",  2, compte_int(t1, 5,  2));
    printf("%d dans t2 : %d fois.\n", 10, compte_int(t2, 10, 10));
    printf("%d dans t2 : %d fois.\n", 42, compte_int(t2, 10, 42));
}
~~~~

## Problème avec une liste chaînée

_Fichier à fournir_ : `intlist.c`modifié et document `intlist.txt` ou `intlist.md`.

Examinez le code dans le fichier `intlist.c`où une structure
de liste chaînées de valeurs entières est définie.

(vous pouvez répondre à ces questions dans un fichier `intlist.txt`
 ou bien dans des commentaires ajoutés au fichier `intlist.c`).

- Quel problème constatez-vous en exécutant le programme ?
  (note : vous pouvez l'interrompre par CTRL-C)
- Quel est la raison de ce problème ?
- Décrivez un algorithme permettant de détecter une liste
  chaînée ayant ce problème.
- Implémentez cet algorithme dans une fonction et testez-la.

# QCM

Lancement du QCM :

**Aucun document autorisé**

Deux liens :

Mettez votre prénom comme pseudonyme !

Si votre nom de famille commence par une lettre entre A et L :

https://kahoot.it/challenge/06914807?challenge-id=82b41523-9d9b-4e91-a9a2-77753416ec0b_1706532341417

Si votre nom de famille commence par une lettre entre M et Z :

https://kahoot.it/challenge/01657766?challenge-id=82b41523-9d9b-4e91-a9a2-77753416ec0b_1706532393578


